var express = require('express');
var router = express.Router();
var mw = require('../lib/middleware-wrapper');
var demoCtrl = require('../controllers/demo-controller');
var loginCtrl = require('../controllers/login-controller');
var followCtrl = require('../controllers/follow-controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/demo/foo', mw(demoCtrl.foo));

router.post('/api/login', mw(loginCtrl.login));

router.post('/api/logout', mw(loginCtrl.logout));

router.post('/api/follow', mw(followCtrl.followSomeone));

router.post('/api/unfollow', mw(followCtrl.unfollowSomeone));

module.exports = router;
