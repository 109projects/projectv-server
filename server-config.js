var config = {
    defaultExpTime: 1000,
    minExpTime: 5,
    maxExpTime: 100000,
    storagePath: {
        audio: process.cwd() + '/storage/audio/'
    }
};

module.exports = config;