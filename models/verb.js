var seq = require('./seq');
var Sequelize = require('sequelize');

var Verb = seq.define('Verb', {
    id : {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    name : Sequelize.STRING
});

module.exports = Verb;

var Action = require('./action');
Verb.hasMany(Action, {foreignKey: 'verbId'});