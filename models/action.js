var seq = require('./seq');
var Sequelize = require('sequelize');

var Action = seq.define('Action', {
    id : {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    name : Sequelize.STRING,
    icon : Sequelize.STRING,
    verbId: Sequelize.INTEGER
});

module.exports = Action;

var Verb = require('./verb');
Action.belongsTo(Verb, {foreignKey: 'verbId'});
var Feed = require('./feed');
Action.hasMany(Feed, {foreignKey: 'actionId'});