var seq = require('./seq');
var Sequelize = require('sequelize');

var FeedAccess = seq.define('FeedAccess', {
    id : {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    feedId : Sequelize.INTEGER,
    userId : Sequelize.INTEGER,
    accessedTime: Sequelize.DATE
});

module.exports = FeedAccess;

var Feed = require('./feed');
FeedAccess.belongsTo(Feed, {foreignKey: 'feedId'});