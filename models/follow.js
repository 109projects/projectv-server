var seq = require('./seq');
var Sequelize = require('sequelize');

var Follow = seq.define('Follow', {
    id : {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    followerUserId : Sequelize.INTEGER,
    followeeUserId : Sequelize.INTEGER,
    acceptedTime : Sequelize.DATE,
    subscribe: Sequelize.INTEGER(1)
});

module.exports = Follow;

var User = require('./user');
Follow.belongsTo(User, {foreignKey: 'followerUserId', as: 'follower'});
Follow.belongsTo(User, {foreignKey: 'followeeUserId', as: 'followee'});