var Follow = require('../models/follow');
var User = require('../models/user');

exports.subscribeSomeone = function (req, res, promise) {
    var respond = {
        err: null
    };

    if (req.body.followeeToBeSubscribeUserId == null || req.body.followeeToBeSubscribeUserId == undefined || req.body.followeeToBeSubscribeUserId == '') {
        promise.reject('followeeUserIdCannotBeEmpty');
        return promise;
    }

    if ( typeof req.body.followeeToBeSubscribeUserId != "number") {
        promise.reject('followeeUserIdShouldBeNumber');
        return promise;
    }

    Follow.update({subscribe: 1}, {where:{
        followerUserId: req.locals.user.id,
        followeeUserId: req.body.followeeToBeSubscribeUserId,
        acceptedTime: {
            $ne: null
        }
    }}).then(([noOfAffectedRow])=>{
        if (noOfAffectedRow != 1) {
            throw 'followNotAcceptedOrNotExist';
        }
        res.send(respond);
        promise.resolve();
    }).catch(function (e) {
        promise.reject(e);
    });
    return promise;
};

exports.unsubscribeSomeone = function (req, res, promise) {
    var respond = {
        err: null
    };

    if (req.body.followeeToBeUnsubscribeUserId == null || req.body.followeeToBeUnsubscribeUserId == undefined || req.body.followeeToBeUnsubscribeUserId == '') {
        promise.reject('followeeUserIdCannotBeEmpty');
        return promise;
    }

    if ( typeof req.body.followeeToBeUnsubscribeUserId != "number") {
        promise.reject('followeeUserIdShouldBeNumber');
        return promise;
    }

    Follow.update({subscribe: 0}, {where:{
        followerUserId: req.locals.user.id,
        followeeUserId: req.body.followeeToBeUnsubscribeUserId,
        acceptedTime: {
            $ne: null
        }
    }}).then(([noOfAffectedRow])=>{
        console.log(noOfAffectedRow);
        if (noOfAffectedRow != 1) {
            throw 'followNotAcceptedOrNotExist';
        }
        res.send(respond);
        promise.resolve();
    }).catch(function (e) {
        promise.reject(e);
    });
    return promise;
};