var Follow = require('../models/follow');
var User = require('../models/user');
var sockServerApi = require('../lib/socket-server-api');

exports.followSomeone = function (req, res, promise) {
    /* req.body = {
     followeeUserId,
     followerToken*****
     */
    var respond = {
        err : null
    };

    if (req.body.followeeUserId == null || req.body.followeeUserId == undefined || req.body.followeeUserId == '') {
        promise.reject('followeeUserIdCannotBeEmpty');
        return promise;
    }

    if ( typeof req.body.followeeUserId != "number") {
        promise.reject('followeeUserIdShouldBeNumber');
        return promise;
    }

    User.findOne({where: {id: req.body.followeeUserId}})
        .then(function (followeeUser) {
            if (followeeUser == null) {
                throw 'followeeUserNotFound';
            } else
                return Promise.resolve(followeeUser);
        })
        .then(function (followeeUser) {
            return Follow.create({
                followerUserId: req.locals.user.id,
                followeeUserId: followeeUser.id,
                acceptedTime: null,
                subscribe: 0
            })
        })
        .then(function (follow) {
            return sockServerApi.notifyFollowingRequest(); //todo: finish socket server part
        })
        .then(function () {
            res.send(respond);
            promise.resolve();
        })
        .catch(function (e) {
            promise.reject(e);
        });
    return promise;

};

exports.unfollowSomeone = function (req, res, promise) {
    /* req.body = {
     followeeUserId,
     followerToken
     */

    var respond = {
        err : null
    };

    if (req.body.followeeUserId == null || req.body.followeeUserId == undefined || req.body.followeeUserId == '') {
        promise.reject('followeeUserIdCannotBeEmpty');
        return promise;
    }

    if ( typeof req.body.followeeUserId != "number") {
        promise.reject('followeeUserIdShouldBeNumber');
        return promise;
    }

    Follow.destroy({where: {followeeUserId: req.body.followeeUserId, followerUserId: req.locals.user.id}})
        .then(function (noOfAffectedRow) {
            if (noOfAffectedRow != 1) {
                throw 'followNotFound';
            }
            res.send(respond);
            promise.resolve();
        })
        .catch(function (e) {
            promise.reject(e);
    });
    return promise;
};

exports.acceptFollow = function (req, res, promise) {
    var respond = {
        err: null
    };
    var now = Date.now();
    req.locals.now = now;

    if (req.body.followerToBeAcceptedUserId == null || req.body.followerToBeAcceptedUserId == undefined || req.body.followerToBeAcceptedUserId == '') {
        promise.reject('followerUserIdCannotBeEmpty');
        return promise;
    }

    if ( typeof req.body.followerToBeAcceptedUserId != "number") {
        promise.reject('followerUserIdShouldBeNumber');
        return promise;
    }

    Follow.update({acceptedTime: now}, {
            where: {
                followerUserId: req.body.followerToBeAcceptedUserId,
                followeeUserId: req.locals.user.id
            }
        })
        .then(function () {
            res.send(respond);
            promise.resolve();
        })
        .catch(function (e) {
            console.log(e);
        });
    return promise;
};