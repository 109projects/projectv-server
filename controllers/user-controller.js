var User = require('../models/User');

exports.search = function(req, res, promise) {
    var searchWord = req.body.searchWord;
    if (searchWord === undefined || searchWord === null || searchWord === '') {
        // Do not allow searching all users
        res.json({
            err: null,
            data: {
                users: []
            }
        });
        promise.resolve();
    }
    else {
        //searching with criteria
        User.findAll({
            where: {$or:[
                {username: {$like: '%' + searchWord + '%'}},
                {email: {$like: '%' + searchWord + '%'}}
            ]}
        }).then(function(users) {
            var retUsernames = users.map(function(ele) {
                //TODO: should not only return username
                return ele.username;
            });
            res.json({
                err: null,
                data: {
                    users: retUsernames
                }
            });
            promise.resolve();
        });
    }

    return promise;
};