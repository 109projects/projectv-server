var Feed = require('../models/feed');
var utils = require('../lib/utils');
var serverConfig = require('../server-config');
var fs = require('bluebird').promisifyAll(require('fs'));
var socketServerApi = require('../lib/socket-server-api');

exports.push = function (req, res, promise) {
    var actionId = req.body.actionId;
    var customAction = req.body.customAction;
    //actionId validation is performed using the database
    if (!utils.strNotEmpty(customAction) && customAction !== null && customAction !== undefined) {
        promise.reject('invalid customAction');
        return promise;
    }
    if (!utils.isset(actionId)
        && !utils.isset(customAction)) {
        promise.reject('both actionId and customAction are missing');
        return promise;
    }
    if (utils.isset(actionId)
        && utils.isset(customAction)) {
        promise.reject('both actionId and customAction exists');
        return promise;
    }
    if (!utils.isset(req.body.access) || !(req.body.access instanceof Array)) {
        promise.reject('invalid access');
    }

    var now = Date.now();
    var ti = req.body.expiryTime - now;
    if (!( ti >= serverConfig.minExpTime
        && ti <= serverConfig.maxExpTime)) {
        req.body.expiryTime = now + serverConfig.defaultExpTime;
        res.testing = {// for test script, do not remove
            now: now
        }
    }

    if (!utils.strNotEmpty(req.body.audio)) {
        promise.reject('invalid audio');
        return promise;
    }

    var accessors = req.body.access;
    var audioName;
    req.locals.user.getFollowerFollows({where: {acceptedTime: {$ne: null}}}).then(function (follows) {
        if (accessors.length > follows.length)  //prevent huge data in req.access to attack server
            throw 'invalid access';
        var ids = follows.map(function (f) {
            return f.followerUserId
        });
        if (accessors.length === 0)
            accessors = ids;
        else {
            accessors.forEach(function (a, i) {
                var index = ids.indexOf(a);
                if (index !== i     //accessor is not a follower or has duplicated accessors
                    || a === req.locals.user.id) {  //trying to grant access right to user him/herself
                    throw 'invalid access';
                }
            });
        }

        audioBinary = new Buffer(req.body.audio, 'base64');

        audioName = Date.now();
        var audioPath = serverConfig.storagePath.audio + audioName;
        return fs.writeFileAsync(audioPath, audioBinary);
    }).then(function () {
        return Feed.create({
            userId: req.locals.user.id,
            actionId: req.body.actionId,
            customAction: req.body.customAction,
            audio: audioName,
            expiryTime: req.body.expiryTime
        })
    }).then(function (feed) {
        return feed.addAccessibleUsers(accessors).then(function () {
            socketServerApi.notifyNewFeed(feed);
            res.json({
                err: null,
                data: {
                    feedId: feed.id
                }
            });
            promise.resolve();
        });
    }).catch(function (e) {
        if (e.name === 'SequelizeForeignKeyConstraintError')
            promise.reject('invalid actionId');
        else
            promise.reject(e);
    });

    return promise;
};

var User = require('../models/user');
exports._getNewFeeds = (user, lastSyncTime, now) => {
    return user.getAccessibleFeeds({
        where: {
            expiryTime: {
                $gt: now
            }
        },
        include: [User],
        through: {
            where: {
                accessedTime: null,
                createdAt: {
                    $gte: lastSyncTime
                },
            }
        }
    });
};

exports._getFollowRequests = (user, lastSyncTime) => {
    return user.getFolloweeFollows({
        where: {
            acceptedTime: null,
            createdAt: {
                $gte: lastSyncTime
            }
        }
    });
};

var FeedAccess = require('../models/feed-access');
var seq = require('../models/seq');
exports._markFeedsAsAccessed = (user, feedIds, now) => {
    if (!(feedIds instanceof Array))
        return Promise.reject('invalid accessedFeedIds');
    if (feedIds.length === 0)
        return Promise.resolve();
    return seq.transaction((t) => (
        FeedAccess.update({
            accessedTime: now
        }, {
            where: {
                feedId: {$in: feedIds},
                userId: user.id,
                accessedTime: null
            },
            paranoid: false,
            transaction: t
        }).then(([affectedRows]) => {
            if (affectedRows !== feedIds.length)
                throw 'invalid accessedFeedIds';
        })
    ))
};

exports._getFeedsWithRemovedFeedAccess = (user, lastSyncTime, now) => {
    return user.getFeedAccesses({
        where: {
            createdAt: {
                $lt: lastSyncTime
            },
            deletedAt: {
                $gte: lastSyncTime
            },
        },
        paranoid: false
    }).then((feedAccesses) =>
        Feed.findAll({
            where: {
                id: {
                    $in: feedAccesses.map((f) => f.feedId)
                }
            }
        })
    )
};

exports._getExpiredFeeds = (user, lastSyncTime, now) => {
    return user.getAccessibleFeeds({
        where: {
            expiryTime: {
                $gte: lastSyncTime,
                $lte: now
            },
        }
    })
};

exports.getFeedUpdates = (user, accessedFeedIds, lastSyncTime, now) => {
    var add = [];
    var remove = [];
    return exports._markFeedsAsAccessed(user, accessedFeedIds, now)
        .then(() =>
            Promise.all([
                exports._getFeedsWithRemovedFeedAccess(user, lastSyncTime, now)
                    .then((feedsWifRemovedFa) => {
                        remove = remove.concat(feedsWifRemovedFa)
                    }),
                exports._getExpiredFeeds(user, lastSyncTime, now)
                    .then((expiredFeeds) => {
                        remove = remove.concat(expiredFeeds)
                    }),
                exports._getNewFeeds(user, lastSyncTime, now)
                    .then((newFeeds) => {
                        add = add.concat(newFeeds);
                    })
            ])
        ).then(() => {
            remove.map((f) => f.id).concat(accessedFeedIds);
            var finalRemove = accessedFeedIds;
            remove.forEach(function(f) {
                if (finalRemove.indexOf(f.id) === -1)
                    finalRemove.push(f.id)
            });
            finalRemove.sort(); //todo: no need to sort
            return Promise.resolve({
                feeds: {
                    add: add.map(f => ({
                        id: f.id,
                        userId: f.User.id,
                        username: f.User.username,
                        phrase: '',
                        expiryTime: f.expiryTime.getTime()
                    })),
                    remove: finalRemove
                }
            })
        })
};
