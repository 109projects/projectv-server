var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var httpMock = require('node-mocks-http');
var userCtrl = require('../../controllers/user-controller');
var rPromise = require('../../lib/resolvable-promise');
var seq = require('../../models/seq');
var User = require('../../models/user');
var seqUtils = require('../../lib/seq-utils');

context('user-controller.search()', function () {
    before(function () {
        return seqUtils.truncate(seq, User).then(function() {
            return User.bulkCreate([
                {
                    username: 'foobar2000',
                    email: 'abc@gmail.com'
                },
                {
                    username: 'belloworld',
                    email: 'foobar2000@gmail.com'
                },
                {
                    username: 'not_this',
                    email: 'not_this@gmail.com'
                }
            ])
        })
    });

    it('should return user matching the criteria', function () {
        var req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/user/search',
            body: {
                searchWord: 'foobar'
            }
        });
        var res = httpMock.createResponse();
        return userCtrl.search(req, res, rPromise()).then(function () {
            expect(res.statusCode).to.equal(200);
            var data = JSON.parse(res._getData());
            expect(data).to.eql({
                err: null,
                data: {
                    users: ['foobar2000', 'belloworld']
                }
            })
        })
    });

    it('should not return any users', function() {
        var req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/user/search',
            body: {
                searchWord: ''
            }
        });
        var res = httpMock.createResponse();
        return userCtrl.search(req, res, rPromise()).then(function () {
            expect(res.statusCode).to.equal(200);
            var data = JSON.parse(res._getData());
            expect(data).to.eql({
                err: null,
                data: {
                    users: []
                }
            })
        })
    });

    after(function () {
        return seqUtils.truncate(seq, User);
    });
});