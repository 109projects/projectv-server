var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var httpMock = require('node-mocks-http');
var request = require('request-promise');
var rPromise = require('../../lib/resolvable-promise');

// lib
var hasher = require('../../lib/hasher');
var testUtils = require('../../lib/test-utils');
var seqUtils = require('../../lib/seq-utils');

// controller
var subscribeCtrl = require('../../controllers/subscribe-controller');

// model
var seq = require('../../models/seq');
var Follow = require('../../models/follow');
var User = require('../../models/user');

context('subscribe-controller.unsubscribeSomeone()', function () {
    var req, res, now,
        nonFolloweeUser = User.build();

    now = Date.now();

    beforeEach(function () {
        req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/follow',
            locals: {
                user: {
                    id: 1
                }
            }
        });
        res = httpMock.createResponse();

        return seqUtils.truncate(seq, User, Follow).then(function () {
            return Promise.all([
                User.create({
                    username: 'abc',
                    password: hasher.hash('abc', hasher.hashVal.dbPw),
                    email: 'abc@gmail.com'
                }),
                User.create({
                    username: 'ToBeFollowed',
                    password: hasher.hash('cba', hasher.hashVal.dbPw),
                    email: 'ToBeFollowed@gmail.com'
                }),
                nonFolloweeUser.save()
            ])
        }).then(function () {
            return Follow.create({
                followerUserId: 1,
                followeeUserId: 2,
                acceptedTime: now,
                subscribe: 1
            });
        }).then(function () {
            console.log('===============test start==================');
        });
    });

    afterEach(function () {
        return seqUtils.truncate(seq, User, Follow);
    });

    //unit test: normal case
    it('noraml parameter - unsubscribe succeed and return no error', function () {
        req.body = {
            followeeToBeUnsubscribeUserId: 2
        };
        return subscribeCtrl.unsubscribeSomeone(req, res, rPromise()).then(function () {
            expect(res.statusCode).to.equal(200);
            expect(res._getData()).to.eql({
                err: null
            });
            return Follow.findAll();
        }).then(function (follows) {
            expect(follows.length).to.equal(1);
            expect(follows[0].followerUserId).to.equal(1);
            expect(follows[0].followeeUserId).to.equal(2);
            expect(follows[0].acceptedTime.getTime()).to.closeTo(now, 1000);
            expect(follows[0].subscribe).to.equal(0);
        });
    });

    //followee not found
    it('followee not found - subscribe fail and return error', function () {
        req.body = {
            followeeToBeUnsubscribeUserId: 100
        };
        var p = subscribeCtrl.unsubscribeSomeone(req, res, rPromise());
        return expect(p).to.be.rejectedWith('followNotAcceptedOrNotExist').then(function () {
            return Follow.findAll();
        }).then(function (follows) {
            expect(follows.length).to.equal(1);
            expect(follows[0].followerUserId).to.equal(1);
            expect(follows[0].followeeUserId).to.equal(2);
            expect(follows[0].acceptedTime.getTime()).to.closeTo(now, 1000);
            expect(follows[0].subscribe).to.equal(1);
        });
    });

    //followee not accepted
    it('followee not accepted - subscribe fail and return error', function () {
        req.body = {
            followeeToBeUnsubscribeUserId: 2
        };
        return Follow.update({acceptedTime: null}, {
            where: {
                followerUserId: 1,
                followeeUserId: 2
            }
        }).then(function () {
            return expect(subscribeCtrl.unsubscribeSomeone(req, res, rPromise())).to.be.rejectedWith('followNotAcceptedOrNotExist');
        }).then(function () {
            return Follow.findAll();
        }).then(function (follows) {
            expect(follows.length).to.equal(1);
            expect(follows[0].followerUserId).to.equal(1);
            expect(follows[0].followeeUserId).to.equal(2);
            expect(follows[0].acceptedTime).to.equal(null);
            expect(follows[0].subscribe).to.equal(1);
        });
    });

    //unit test: followeeId invalid
    context('invalid followeeId - return err', function () {
        testUtils.genTests({
            'null': [null,'followeeUserIdCannotBeEmpty'],
            'undefined': [undefined,'followeeUserIdCannotBeEmpty'],
            'wrong type': ['I am the king of the world!','followeeUserIdShouldBeNumber'],
            'empty string': ['','followeeUserIdCannotBeEmpty'],
            'non followee' : [nonFolloweeUser.id, 'some error message'] //an existing user but not followed by the authedUser
        }, function ([followeeUserId,err]) {
            req.body = {
                followeeToBeUnsubscribeUserId: followeeUserId
            };
            req.locals = {
                userId: 1
            };
            var p = subscribeCtrl.unsubscribeSomeone(req, res, rPromise());
            return expect(p).to.be.rejectedWith(err).then(function () {
                return Follow.findAll();
            }).then(function (follows) {
                expect(follows.length).to.equal(1);
                expect(follows[0].followerUserId).to.equal(1);
                expect(follows[0].followeeUserId).to.equal(2);
                expect(follows[0].acceptedTime.getTime()).to.closeTo(now, 1000);
                expect(follows[0].subscribe).to.equal(1);
            });
        });
    });

});
