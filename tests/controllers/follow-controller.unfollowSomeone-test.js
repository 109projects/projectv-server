var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var httpMock = require('node-mocks-http');
var request = require('request-promise');
var rPromise = require('../../lib/resolvable-promise');

var hasher = require('../../lib/hasher');
var seqUtils = require('../../lib/seq-utils');
var testUtils = require('../../lib/test-utils');

var followCtrl = require('../../controllers/follow-controller');

var seq = require('../../models/seq');
var Follow = require('../../models/follow');
var User = require('../../models/user');

context('follow-controller.unfollowSomeone()', function () {

    var req, res,
        notFollowingUser = User.build();

    beforeEach(function () {
        req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/unfollow',
            locals: {
                user: {
                    id: 1
                }
            }
        });
        res = httpMock.createResponse();

        return seqUtils.truncate(seq, User, Follow).then(function () {
            return Promise.all([
                User.create({
                    username: 'abc',
                    password: hasher.hash('abc', hasher.hashVal.dbPw),
                    email: 'abc@gmail.com'
                }),
                User.create({
                    username: 'ToBeFollowed',
                    password: hasher.hash('cba', hasher.hashVal.dbPw),
                    email: 'ToBeFollowed@gmail.com'
                }),
                notFollowingUser.save()
            ])
        }).then(function () {
            return Follow.create({
                followerUserId: 1,
                followeeUserId: 2,
                acceptedTime: null,
                subscribe: false
            });
        }).then(function () {
            console.log('===============test start==================');
        });
    });

    afterEach(function () {
        return seqUtils.truncate(seq, User, Follow);
    });

    //unit test: normal case
    it('noraml parameter - unfollow succeed and return no error', function () {
        req.body = {
            followeeUserId: 2
        };
        return followCtrl.unfollowSomeone(req, res, rPromise()).then(function () {
            expect(res.statusCode).to.equal(200);
            expect(res._getData()).to.eql({
                err: null
            });
            return Follow.findAll();
        }).then(function (follows) {
            expect(follows.length).to.equal(0);
        });
    });

    //unit test: followeeUser is empty
    context('invalid parameter - return err and remove no follow', function () {
        testUtils.genTests({
            'user not found': 100,
            'null': null,
            'undefined': undefined,
            'wrong type': 'I am the king of the world!',
            'empty string': '',
            'not following user': notFollowingUser.id
        }, function (followeeUserId) {
            req.body = {
                followeeUserId: followeeUserId
            };
            var p = followCtrl.unfollowSomeone(req, res, rPromise());
            return expect(p).to.be.rejected.then(function () {
                return p;
            }).catch(function () {
                return Follow.findAll();
            }).then(function (follows) {
                expect(follows.length).to.equal(1);
            });
        });
    });
});