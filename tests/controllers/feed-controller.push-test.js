var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var rPromise = require('../../lib/resolvable-promise');
var httpMock = require('node-mocks-http');
var sinon = require('sinon');
var socketServerApi = require('../../lib/socket-server-api');
var feedCtrl = require('../../controllers/feed-controller');

var seq = require('../../models/seq');
var Feed = require('../../models/feed');
var User = require('../../models/user');
var Follow = require('../../models/follow');
var Action = require('../../models/Action');
var FeedAccess = require('../../models/feed-access');

var seqUtils = require('../../lib/seq-utils');
var utils = require('../../lib/utils');
var testUtils = require('../../lib/test-utils');
var bluebird = require('bluebird');
var fs = bluebird.promisifyAll(require('fs-extra'));

var serverConfig = require('../../server-config');

context('feed-controller.push()', function () {
    var req, res, now, expTime,
        authedUser = User.build({}), follower1 = User.build({}), follower2 = User.build({}),
        nonAcceptedFollower = User.build({}), nonFollower = User.build({}),
        poisonPenis = User.build({}),    //poisonPenis has no followers
        dummyAction = Action.build({});

    before(function () {
        return seqUtils.truncate(seq, User, Follow).then(function () {
            return Promise.all([
                authedUser.save(),
                follower1.save(),
                follower2.save(),
                nonAcceptedFollower.save(),
                nonFollower.save(),
                poisonPenis.save(),
                dummyAction.save()
            ]).then(function () {
                authedUser.addFollowers([follower1, follower2], {
                    acceptedTime: Date.now()
                });
                authedUser.addFollower(nonAcceptedFollower, {
                    acceptedTime: null
                });
            });
        });
    });

    after(function () {
        return seqUtils.truncate(seq, User, Follow, Action);
    });

    beforeEach(function () {
        req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/feed/push',
            locals: {
                user: authedUser
            }
        });
        res = httpMock.createResponse();
        now = Date.now();
        expTime = now + 5000;
        return Promise.all([
            seqUtils.truncate(seq, Feed, FeedAccess),
            fs.emptyDirAsync(serverConfig.storagePath.audio)
        ]);
    });

    afterEach(function () {
        return Promise.all([
            seqUtils.truncate(seq, Feed, FeedAccess),
            fs.emptyDirAsync(serverConfig.storagePath.audio)
        ]);
    });

    context('normal parameters - creates a feed in db and save audio in filesystem', function() {
        beforeEach(function() {
            sinon.stub(socketServerApi, 'notifyNewFeed').returns(Promise.resolve());
        });
        afterEach(function() {
            socketServerApi.notifyNewFeed.restore();
        });

        testUtils.genTests({
            'with actionId' : function() {
                return {actionId: dummyAction.id, customAction: null}
            },
            'with custom action' : {actionId: null, customAction: 'custom action string'}
        }, function(data) {
            req.body = {
                actionId: data.actionId,
                customAction: data.customAction,
                audio: new Buffer('base64-encoded audio binary').toString('base64'),
                expiryTime: expTime,
                access: [
                    follower1.id, follower2.id
                ]
            };

            return feedCtrl.push(req, res, rPromise()).then(function () {
                expect(res.statusCode).to.equal(200);
                expect(JSON.parse(res._getData())).to.eql({
                    err: null,
                    data: {
                        feedId: 1
                    }
                });
                expect(socketServerApi.notifyNewFeed.callCount).to.equal(1);
                var feed = socketServerApi.notifyNewFeed.args[0][0];
                expect(feed).to.be.an.instanceof(Feed.Instance);
                return Feed.findAll();
            }).then(function (feeds) {
                expect(feeds.length).to.equal(1);
                var f = feeds[0];
                expect(f.id).to.equal(1);
                expect(f.userId).to.equal(authedUser.id);
                expect(f.actionId).to.equal(data.actionId);
                expect(f.customAction).to.equal(data.customAction);
                expect(f.expiryTime.getTime()).to.be.closeTo(expTime, 1000);
                expect(f.audio).to.be.a('string');
                return fs.readFileAsync(serverConfig.storagePath.audio + f.audio).then(function (data) {
                    expect(data.toString()).to.equal('base64-encoded audio binary');
                });
            })
        });
    });

    context('invalid actionId - returns error and no feeds created in db', function () {
        testUtils.genTests({
            'record does not exist': 9999,
            'string': 'I am the king of the world!',
            'empty string': ''
        }, function (actionId) {
            req.body = {
                actionId: actionId,
                customAction: 'custom action string',
                audio: new Buffer('base64-encoded audio binary').toString('base64'),
                expiryTime: expTime,
                access: [
                    follower1.id, follower2.id
                ]
            };
            return expectErrorAndNoFeedsCreated();
        });
    });

    context('invalid customAction - no feeds created', function () {
        testUtils.genTests({
            'empty string': '',
            'number': 1234
        }, function (customAction) {
            req.body = {
                actionId: null,
                customAction: customAction,
                audio: new Buffer('base64-encoded audio binary').toString('base64'),
                expiryTime: expTime,
                access: [
                    follower1.id, follower2.id
                ]
            };
            return expectErrorAndNoFeedsCreated('invalid customAction');
        })
    });

    it('both actionId and customAction are missing - no feeds created in db', function () {
        req.body = {
            actionId: null,
            customAction: null,
            audio: new Buffer('base64-encoded audio binary').toString('base64'),
            expiryTime: expTime,
            access: [
                follower1.id, follower2.id
            ]
        };
        return expectErrorAndNoFeedsCreated('both actionId and customAction are missing');
    });

    it('both actionId and customAction exists - no feeds created in db', function() {
        req.body = {
            actionId: dummyAction.id,
            customAction: 'AwYeah~~',
            audio: new Buffer('base64-encoded audio binary').toString('base64'),
            expiryTime: expTime,
            access: [
                follower1.id, follower2.id
            ]
        };
        return expectErrorAndNoFeedsCreated();
    });

    context('invalid expiryTime - set expiryTime to default', function () {
        testUtils.genTests({
            'null' : null
        }, function(expiryTime) {
            req.body = {
                actionId: null,
                customAction: 'custom action',
                audio: new Buffer('base64-encoded audio binary').toString('base64'),
                expiryTime: expiryTime,
                access: [
                    follower1.id, follower2.id
                ]
            };
            return feedCtrl.push(req, res, rPromise()).then(function () {
                expect(res.statusCode).to.equal(200);
                expect(JSON.parse(res._getData())).to.eql({
                    err: null,
                    data: {
                        feedId: 1
                    }
                });
                return Feed.findAll()
            }).then(function (feeds) {
                expect(feeds.length).to.equal(1);
                var f = feeds[0];
                expect(f.expiryTime.getTime()).to.be.closeTo(res.testing.now + serverConfig.defaultExpTime, 1000);
            })
        });
    });

    context('invalid audio - no feeds created', function () {
        return testUtils.genTests({
            'undefined' : undefined,
            'null' : null,
            'empty string' : '',
            'not string' : 1234
        }, function (audio) {
            req.body = {
                actionId: null,
                customAction: 'custom action',
                audio: audio,
                expiryTime: expTime,
                access: []
            };
            return expectErrorAndNoFeedsCreated('invalid audio');
        });
    });

    //Feed access
    it('creates feed-access records in db', function () {
        req.body = {
            actionId: null,
            customAction: 'custom action string',
            audio: new Buffer('base64-encoded audio binary').toString('base64'),
            expiryTime: expTime,
            access: [
                follower1.id, follower2.id
            ]
        };
        return feedCtrl.push(req, res, rPromise()).then(function () {
            return FeedAccess.findAll()
        }).then(function (feedAccesses) {
            expect(feedAccesses.length).to.equal(2);
            var ids = feedAccesses.map(function (fa) {
                return fa.userId
            });
            expect(ids).to.include.members([follower1.id, follower2.id]);
            for (var i = 0; i < feedAccesses.length; i++)
                expect(feedAccesses[i].accessedTime).to.equal(null)
        })
    });

    context('invalid req.access - no feed created', function () {
        testUtils.genTests({
            'undefined': undefined,
            'null': null,
            'non-array': {0: 1},
            'non-number members in array': function () {
                return [follower1.id.toString()]
            },
            'invalid userId': [9999],
            'non follower': function() {
                return [nonFollower.id];
            },
            'non accepted follower': function () {
                return [nonAcceptedFollower.id]
            },
            'duplicated userId': function () {
                return [follower1.id, follower1.id]
            },
            'user himself/herself' : function() {
                return [authedUser.id]
            }
        }, function (audio) {
            req.body = {
                actionId: null,
                customAction: 'custom action string',
                audio: new Buffer('base64-encoded audio binary').toString('base64'),
                expiryTime: expTime,
                access: audio
            };
            return expectErrorAndNoFeedsCreated('invalid access');
        })
    });

    it('empty array in req.access - feed created with access granted to all accepted followers', function () {
        req.body = {
            actionId: null,
            customAction: 'custom action string',
            audio: new Buffer('base64-encoded audio binary').toString('base64'),
            expiryTime: expTime,
            access: []
        };
        return feedCtrl.push(req, res, rPromise()).then(function () {
            return FeedAccess.findAll();
        }).then(function (feedAccesses) {
            expect(feedAccesses.length).to.equal(2);
            var ids = feedAccesses.map(function (fa) {
                return fa.userId
            });
            expect([follower1.id, follower2.id]).to.include.members(ids);
        });
    });

    it('empty array in req.access but user has no followers - should success', function () {
        req.locals.user = poisonPenis;
        req.body = {
            actionId: null,
            customAction: 'custom action string',
            audio: new Buffer('base64-encoded audio binary').toString('base64'),
            expiryTime: expTime,
            access: []
        };
        return feedCtrl.push(req, res, rPromise()).then(function () {
            return Promise.all([
                Feed.findAll().then(function (feeds) {
                    expect(feeds.length).to.equal(1);
                }),
                FeedAccess.findAll().then(function (feedAccesses) {
                    expect(feedAccesses.length).to.equal(0);
                })
            ])
        })
    });

    function expectErrorAndNoFeedsCreated(err) {
        return testUtils.controllers.expectRejected(feedCtrl.push(req, res, rPromise()), [Feed, FeedAccess], err)
    }
});

