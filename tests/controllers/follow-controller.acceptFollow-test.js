var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var sinon = require('sinon');
var httpMock = require('node-mocks-http');
var request = require('request-promise');
var rPromise = require('../../lib/resolvable-promise');

// lib
var hasher = require('../../lib/hasher');
var testUtils = require('../../lib/test-utils');
var seqUtils = require('../../lib/seq-utils');

// controller
var followCtrl = require('../../controllers/follow-controller');

// model
var seq = require('../../models/seq');
var Follow = require('../../models/follow');
var User = require('../../models/user');

context('follow-controller.acceptFollow', function () {
    var req, res, now;

    beforeEach(function () {
        req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/follow',
            locals: {
                user: {
                    id: 2
                }
            }
        });
        res = httpMock.createResponse();

        return seqUtils.truncate(seq, User, Follow).then(function () {
            return User.create({
                username: 'abc',
                password: hasher.hash('abc', hasher.hashVal.dbPw),
                email: 'abc@gmail.com'
            });
        }).then(function () {
            return User.create({
                username: 'ToBeFollowed',
                password: hasher.hash('cba', hasher.hashVal.dbPw),
                email: 'ToBeFollowed@gmail.com'
            });
        }).then(function () {
            return Follow.create({
                followerUserId: 1,
                followeeUserId: 2,
                acceptedTime: null,
                subscribe: 0
            });
        }).then(function () {
            console.log('===============test start==================');
        });
    });

    afterEach(function () {
        return seqUtils.truncate(seq, User, Follow);
    });

    //unit test: normal case
    context('noraml parameter - accept follow succeed and return no error', () => {
        var socketServerApi = require('../../lib/socket-server-api');
        before(() => {
            sinon.stub(socketServerApi, 'notifyFollowAccepted');
        });
        after(() => {
            socketServerApi.notifyFollowAccepted.restore();
        });

        it('test', function () {
            req.body = {
                followerToBeAcceptedUserId: 1
            };
            return followCtrl.acceptFollow(req, res, rPromise()).then(function () {
                expect(res.statusCode).to.equal(200);
                expect(res._getData()).to.eql({
                    err: null
                });
                expect(socketServerApi.notifyFollowAccepted.callCount).to.equal(1);
                expect(socketServerApi.notifyFollowAccepted.args[0][0]).to.be.instanceof(Follow.Instance);
                return Follow.findAll();
            }).then(function (follows) {
                expect(follows.length).to.equal(1);
                expect(follows[0].followerUserId).to.equal(1);
                expect(follows[0].followeeUserId).to.equal(2);
                expect(follows[0].acceptedTime.getTime()).to.closeTo(req.locals.now, 1000);
                expect(follows[0].subscribe).to.equal(0);
            });
        });
    });

    //unit test: follower has not followed
    it('follow not found - accept follow fail and return error', function () {
        req.body = {
            followerToBeAcceptedUserId: 10
        };
        return followCtrl.acceptFollow(req, res, rPromise()).then(function () {
            expect(res.statusCode).to.equal(200);
            expect(res._getData()).to.eql({
                err: null
            });
            return Follow.findAll();
        }).then(function (follows) {
            expect(follows.length).to.equal(1);
            expect(follows[0].followerUserId).to.equal(1);
            expect(follows[0].followeeUserId).to.equal(2);
            expect(follows[0].acceptedTime).to.equal(null);
            expect(follows[0].subscribe).to.equal(0);
        });
    });

    //unit test: followerId invalid
    context('invalid followerId - return err', function () {
        testUtils.genTests({
            'null': null,
            'undefined': undefined,
            'wrong type': 'I am the king of the world!',
            'empty string': ''
        }, function (followerUserId) {
            req.body = {
                followerToBeAcceptedUserId: followerUserId
            };
            var p = followCtrl.acceptFollow(req, res, rPromise());
            return expect(p).to.be.rejected.then(function () {
                return p;
            }).catch(function () {
                return Follow.findAll();
            }).then(function (follows) {
                expect(follows.length).to.equal(1);
                expect(follows[0].followerUserId).to.equal(1);
                expect(follows[0].followeeUserId).to.equal(2);
                expect(follows[0].acceptedTime).to.equal(null);
                expect(follows[0].subscribe).to.equal(0);
            });
        });
    });
});