var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var sinon = require('sinon');
var seq = require('../../models/seq');
var testUtils = require('../../lib/test-utils');
var seqUtils = require('../../lib/seq-utils');

var User = require('../../models/user');
var Feed = require('../../models/feed');
var FeedAccess = require('../../models/feed-access');
var Follow = require('../../models/follow');

var feedCtrl = require('../../controllers/feed-controller');

context('feed-controller.pull()', function () {
    /* TODO
     return listened feedIds, new feeds, friend requests, feed access updates, map id
     sync-controller: check lastSyncTime < now;
     move _getFollowRequest() to follow controller._getNewFollowRequests, removed follow request
     */
    var lastSyncTime = Math.round(Date.now() / 1000) * 1000,
        now = lastSyncTime + 10000,
        authedUser = User.build(),
        feedOwner = User.build();


    before(() => seqUtils.truncate(seq, User, Feed, FeedAccess)
        .then(() => seqUtils.save(authedUser, feedOwner))
    );

    after(() => (seqUtils.truncate(seq, User)));

    context('_getFollowRequests()', () => {
        var followReq = Follow.build({});
        before(() => (
            seqUtils.truncate(seq, Follow).then(() => (
                seqUtils.save(followReq)
            )).then(() => (
                authedUser.addFolloweeFollow(followReq)
            ))
        ));
        after(() => (seqUtils.truncate(seq, Follow)));

        it('returns pending follow requests', () =>
            feedCtrl._getFollowRequests(authedUser, lastSyncTime).then((followRequests) => {
                expect(followRequests.length).to.equal(1);
                expect(followRequests[0].id).to.equal(followReq.id);
            })
        );

        it('follow requests does not belongs to user should not be returned', () =>
            Follow.create({createdAt: lastSyncTime + 1000})
                .then(() => feedCtrl._getFollowRequests(authedUser, lastSyncTime))
                .then((followRequests) => {
                    expect(followRequests.length).to.equal(1);
                    expect(followRequests[0].id).to.equal(followReq.id);
                })
        );

        testUtils.genTests({
                'accepted feeds should not be returned': {acceptedTime: 1, createdAt: lastSyncTime + 1000},
                'downloaded feeds should not be returned': {createdAt: lastSyncTime - 1000}
            }, (data) =>
                Follow.create(data)
                    .then((f) => authedUser.addFolloweeFollow(f))
                    .then(() => feedCtrl._getFollowRequests(authedUser, lastSyncTime))
                    .then((followRequests) => {
                        expect(followRequests.length).to.equal(1);
                        expect(followRequests[0].id).to.equal(followReq.id);
                    })
        )
    });

    context('_getNewFeeds()', () => {
        var feed = Feed.build({
            expiryTime: now + 1000
        });
        before(() => feed.save()
            .then(() => feedOwner.addFeed(feed))
            .then(() => authedUser.addAccessibleFeed(feed))
        );
        after(() => seqUtils.truncate(seq, Feed, FeedAccess));

        it('returns accessible feeds', () => {
            return feedCtrl._getNewFeeds(authedUser, lastSyncTime, now).then((feeds) => {
                expect(feeds.length).to.equal(1);
                var f = feeds[0];
                expect(f.id).to.equal(feed.id);
                expect(f.User.id).to.equal(feedOwner.id);
            });
        });

        it('non accessible feeds should not be returned', () => (
            Feed.create({
                expiryTime: now + 1000
            })
                .then(() => feedCtrl._getNewFeeds(authedUser, lastSyncTime, now))
                .then((feeds) => {
                    expect(feeds.length).to.equal(1);
                    expect(feeds[0].id).to.equal(feed.id);
                })
        ));

        testUtils.genTests({
            'old feeds should not be returned': {
                feed: {
                    expiryTime: now + 1000
                },
                feedAccess: {
                    createdAt: lastSyncTime - 1000,
                }
            },
            'expired feeds should not be returned': {
                feed: {
                    expiryTime: now - 1000
                },
                feedAccess: {
                    createdAt: lastSyncTime + 1000,
                }
            },
            'accessedFeed should not be returned': {
                feed: {
                    expiryTime: now + 1000
                },
                feedAccess: {
                    accessedTime: 1,
                    createdAt: lastSyncTime + 1000,
                }
            },
            'feed with feedAccess removed should not be returned': {
                feed: {
                    expiryTime: now + 1000
                },
                feedAccess: {
                    createdAt: lastSyncTime + 1000,
                    deletedAt: 1
                }
            }
        }, (data) => {
            return Feed.create(data.feed)
                .then((f) => authedUser.addAccessibleFeed(f, data.feedAccess))
                .then(() => feedCtrl._getNewFeeds(authedUser, lastSyncTime, now))
                .then((feeds) => {
                    expect(feeds.length).to.equal(1);
                    expect(feeds[0].id).to.equal(feed.id);
                })
        });
    });

    context('_markFeedsAsAccessed()', () => {
        var feed1;
        beforeEach(() => seqUtils.truncate(seq, Feed, FeedAccess)
            .then(() => Feed.create())
            .then((f) => {
                feed1 = f
            })
            .then(() => authedUser.addAccessibleFeed(feed1))
        );
        afterEach(() => seqUtils.truncate(seq, Feed, FeedAccess));

        it('returns a fulfilled promise', () =>
            feedCtrl._markFeedsAsAccessed(authedUser, [feed1.id], now)
                .then(() => feed1.getFeedAccesses({where: {userId: authedUser.id}}))
                .then((fa) => {
                    expect(fa[0].accessedTime.getTime()).to.closeTo(now, 1000);
                })
        );

        it('multiple feeds', () => {
            var feed2 = Feed.build({});
            return feed2.save()
                .then(() => authedUser.addAccessibleFeed(feed2))
                .then(() => feedCtrl._markFeedsAsAccessed(authedUser, [feed1.id, feed2.id], now))
                .then(() => authedUser.getFeedAccesses({where: {feedId: {$in: [feed1.id, feed2.id]}}}))
                .then((fas) => {
                    fas.forEach((fa) => {
                        expect(fa.accessedTime.getTime()).to.closeTo(now, 1000);
                    })
                })
        });

        it('empty array - returns a resolved promise', () => (
            expect(feedCtrl._markFeedsAsAccessed(authedUser, [], now)).to.be.fulfilled
        ));

        it('feed with feed access deleted - should still be updated', () => {
            var feed = Feed.build({}), feedAccess;
            return seqUtils.save(feed)
                .then(() => authedUser.addAccessibleFeed(feed))
                .then(() => authedUser.getFeedAccesses({
                    where: {
                        feedId: feed.id
                    }
                })).then((fas) => feedAccess = fas[0])
                .then(() => feedAccess.destroy())
                .then(() => feedCtrl._markFeedsAsAccessed(authedUser, [feed.id], now))
                .then(() => feedAccess.reload({paranoid: false}))
                .then(() => {
                    expect(feedAccess.accessedTime.getTime()).to.equal(now)
                })
        });

        it('feed owned by user himself/herself', () => {
            var feed = Feed.build();
            feed.save()
                .then(() => authedUser.addFeed(feed))
                .then(() => (expect(feedCtrl._markFeedsAsAccessed(authedUser, [feed.id], now)))
                    .to.be.rejectedWith('invalid accessedFeedIds'))
        });

        context('invalid accessedFeedIds', () => {
            it('invalid feedId', () =>
                expect(feedCtrl._markFeedsAsAccessed(authedUser, [9999], now)).to.be.rejectedWith('invalid accessedFeedIds')
            );

            it('feed already accessed', () => {
                var accessedFeed = Feed.build({});
                return accessedFeed.save()
                    .then(() => authedUser.addAccessibleFeed(accessedFeed, {accessedTime: 1}))
                    .then(() => (expect(feedCtrl._markFeedsAsAccessed(authedUser, [accessedFeed.id], now))
                        .to.be.rejectedWith('invalid accessedFeedIds')))
            });

            it('feed not accessible by user', () => {
                var nonAccessibleFeed = Feed.build({});
                return nonAccessibleFeed.save()
                    .then(() => (expect(feedCtrl._markFeedsAsAccessed(authedUser, [nonAccessibleFeed.id], now)))
                        .to.be.rejectedWith('invalid accessedFeedIds'))
            });

            testUtils.genTests({
                'undefined': undefined,
                'null': null,
                'non array': 1
            }, (data) => (
                expect(feedCtrl._markFeedsAsAccessed(authedUser, data, now))
                    .to.be.rejectedWith('invalid accessedFeedIds')
            ))
        });
    });

    context('_getFeedsWithRemovedFeedAccess()', () => {
        beforeEach(() => seqUtils.truncate(seq, Feed, FeedAccess));
        afterEach(() => seqUtils.truncate(seq, Feed, FeedAccess));

        it('return feeds removed after lastSyncTime', () => {
            var removedFeed = Feed.build({expiryTime: now + 1000});
            return removedFeed.save()
                .then(() => authedUser.addAccessibleFeed(removedFeed, {
                    createdAt: lastSyncTime - 1000,
                    deletedAt: lastSyncTime + 1000
                }))
                .then(() => feedCtrl._getFeedsWithRemovedFeedAccess(authedUser, lastSyncTime, now))
                .then((removedFeeds) => {
                    expect(removedFeeds.length).to.equal(1);
                    expect(removedFeeds[0].id).to.equal(removedFeed.id);
                })
        });

        it('feeds not accessible by user should not be returned', () => {
            var feed = Feed.build({expiryTime: now + 1000});
            return feed.save()
                .then(() => feedCtrl._getFeedsWithRemovedFeedAccess(authedUser, lastSyncTime, now))
                .then((removedFeeds) => {
                    expect(removedFeeds.length).to.equal(0);
                })
        });

        testUtils.genTests({
            'feeds removed before lastSyncTime should not be returned': {
                feed: {expiryTime: now + 1000},
                feedAccess: {
                    createdAt: lastSyncTime - 1000,
                    deletedAt: lastSyncTime - 1000
                }
            },
            'feed with feed access not removed should not be returned': {
                feed: {expiryTime: now + 1000},
                feedAccess: {
                    createdAt: lastSyncTime - 1000
                }
            },
            'lastSyncTime < feedAccess.createdAt < feedAccess.deletedAt': {
                feed: {expiryTime: now + 1000},
                feedAccess: {
                    createdAt: lastSyncTime + 1000,
                    deletedAt: lastSyncTime + 1000
                }
            }
        }, (data) => {
            return Feed.create(data.feed)
                .then((feed) => authedUser.addAccessibleFeed(feed, data.feedAccess))
                .then(() => feedCtrl._getFeedsWithRemovedFeedAccess(authedUser, lastSyncTime, now))
                .then((removedFeeds) => {
                    expect(removedFeeds.length).to.equal(0);
                })
        });

    });

    context('_getExpiredFeeds()', () => {
        beforeEach(() => seqUtils.truncate(seq, Feed, FeedAccess));
        afterEach(() => seqUtils.truncate(seq, Feed, FeedAccess));

        it('returns lastSyncTime <= expiryTime <= now', () => {
            var feed = Feed.build({expiryTime: lastSyncTime + 1000});
            return feed.save()
                .then(() => authedUser.addAccessibleFeed(feed))
                .then(() => feedCtrl._getExpiredFeeds(authedUser, lastSyncTime, now))
                .then((expiredFeeds) => {
                    expect(expiredFeeds.length).to.equal(1);
                    expect(expiredFeeds[0].id).to.equal(feed.id);
                })
        });

        testUtils.genTests({
            'expiryTime < lastSyncTime should not be returned': {
                expiryTime: lastSyncTime - 1000
            },
            'non-expired feeds should not be returned': {
                expiryTime: now + 1000
            }
        }, (data) => (
            Feed.create(data)
                .then((feed) => authedUser.addAccessibleFeed(feed))
                .then(() => feedCtrl._getExpiredFeeds(authedUser, lastSyncTime, now))
                .then((expiredFeeds) => {
                    expect(expiredFeeds.length).to.equal(0);
                })
        ))
    });

    context('getFeedUpdates()', () => {
        beforeEach(() => seqUtils.truncate(seq, Feed, FeedAccess));
        afterEach(() => {
            feedCtrl._getNewFeeds.restore();
            feedCtrl._markFeedsAsAccessed.restore();
            feedCtrl._getFeedsWithRemovedFeedAccess.restore();
            feedCtrl._getExpiredFeeds.restore();
            return seqUtils.truncate(seq, Feed, FeedAccess)});

        it('concat results from 4 other private functions', () => {
            var feed = Feed.build({
                expiryTime: now + 1000
            });
            sinon.stub(feedCtrl, '_getNewFeeds', () => Promise.resolve([feed]));
            sinon.stub(feedCtrl, '_markFeedsAsAccessed', () => Promise.resolve());
            sinon.stub(feedCtrl, '_getFeedsWithRemovedFeedAccess', () => Promise.resolve([
                {id: 1},
                {id: 2},
                {id: 3}
            ]));
            sinon.stub(feedCtrl, '_getExpiredFeeds', () => Promise.resolve([
                {id: 3},
                {id: 4},
                {id: 5}
            ]));

            return feed.save()
                .then(() => feedOwner.addFeeds(feed))
                .then(() => feed.reload({include: [User]}))
                .then(() => feedCtrl.getFeedUpdates(authedUser, [5, 6, 7], lastSyncTime, now))
                .then((ops) => {
                    expect(ops).to.eql({
                        feeds: {
                            add: [
                                {
                                    id: feed.id,
                                    userId: feed.User.id,
                                    username: feed.User.username,
                                    phrase: '',
                                    expiryTime: now + 1000
                                }
                            ],
                            remove: [
                                1, 2, 3, 4, 5, 6, 7
                            ]
                        }
                    })
                })
        });

        it('one of the function is rejected', () => {
            sinon.stub(feedCtrl, '_getNewFeeds', () => Promise.reject('some error'));
            sinon.stub(feedCtrl, '_markFeedsAsAccessed', () => Promise.resolve());
            sinon.stub(feedCtrl, '_getFeedsWithRemovedFeedAccess', () => Promise.resolve([]));
            sinon.stub(feedCtrl, '_getExpiredFeeds', () => Promise.resolve([]));
            return expect(feedCtrl.getFeedUpdates(authedUser, [], lastSyncTime, now))
                .to.be.rejectedWith('some error');
        })
    })
});

