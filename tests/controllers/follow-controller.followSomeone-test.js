var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;
var sinon = require('sinon');
var httpMock = require('node-mocks-http');
var request = require('request-promise');
var rPromise = require('../../lib/resolvable-promise');

// lib
var hasher = require('../../lib/hasher');
var testUtils = require('../../lib/test-utils');
var seqUtils = require('../../lib/seq-utils');

// controller
var followCtrl = require('../../controllers/follow-controller');

// model
var seq = require('../../models/seq');
var Follow = require('../../models/follow');
var User = require('../../models/user');

context('follow-controller.followSomeone()', function () {
    var req, res, now;

    beforeEach(function () {
        req = httpMock.createRequest({
            method: 'POST',
            uri: '/api/follow',
            locals: {
                user: {
                    id:1
                }
            }
        });
        res = httpMock.createResponse();

        return seqUtils.truncate(seq, User, Follow).then(function () {
            return User.create({
                username: 'abc',
                password: hasher.hash('abc', hasher.hashVal.dbPw),
                email: 'abc@gmail.com'
            });
        }).then(function () {
            return User.create({
                username: 'ToBeFollowed',
                password: hasher.hash('cba', hasher.hashVal.dbPw),
                email: 'ToBeFollowed@gmail.com'
            });
        }).then(function () {
            console.log('===============test start==================');
        });
    });

    afterEach(function () {
        // return seqUtils.truncate(seq, User, Follow);
    });

    //unit test: normal case
    context('noraml parameter - follow succeed and return no error', function() {
        var socketServerApi = require('../../lib/socket-server-api');
        before(() => {
            sinon.stub(socketServerApi, 'notifyFollowingRequest');
        });
        after(() => {
            socketServerApi.notifyFollowingRequest.restore();
        });

        it('test', function () {
            req.body = {
                followeeUserId: 2
            };

            return followCtrl.followSomeone(req, res, rPromise()).then(function () {
                expect(res.statusCode).to.equal(200);
                expect(res._getData()).to.eql({
                    err: null
                });
                expect(socketServerApi.notifyFollowingRequest.callCount).to.equal(1);
                var firstParam = socketServerApi.notifyFollowingRequest.args[0][0];
                expect(firstParam).to.be.instanceof(Follow.Instance);   //notifyFollowingRequest() should be called with a follow
                return Follow.findAll();
            }).then(function (followers) {
                expect(followers.length).to.equal(1);
                expect(followers[0].followerUserId).to.equal(1);
                expect(followers[0].followeeUserId).to.equal(2);
                expect(followers[0].acceptedTime).to.equal(null);
                expect(followers[0].subscribe).to.equal(0);
            })
        });
    });


    //unit test: invalid parameter
    context('invalid parameter - return err and add no follow', function () {
        testUtils.genTests({
            'followeeUser not found': 100,
            'null': null,
            'undefined': undefined,
            'wrong type': 'I am the king of the world!',
            'empty string': '',
            'non number' : '100',
            'following myself': 1
        }, function (followeeUserId) {
            req.body = {
                followeeUserId: followeeUserId
            };
            return testUtils.controllers.expectRejected(followCtrl.followSomeone(req, res, rPromise()), [Follow]);
        });
    });

    it('already followed', () => {
        var alreadyFollowedUser = User.build(),
            authedUser;
        return Promise.all([
            alreadyFollowedUser.save(),
            User.findById(1).then((u) => {authedUser = u})
        ]).then(() => alreadyFollowedUser.addFollowers(authedUser))
            .then(() => {
                req.body = {
                    followeeUserId: alreadyFollowedUser.id
                };
                //no Follows should be created
                return testUtils.controllers.expectRejected(followCtrl.followSomeone(req, res, rPromise()), [Follow])
            })
    });
});