var chai = require('chai');
chai.use(require('chai-as-promised'));
var expect = chai.expect;

context('Demo', function() {
    it('simple test', function() {
        expect(1).to.equal(1);
    });

    context('aync test', function() {
        // http://chaijs.com/plugins/chai-as-promised/
        it('single async test', function() {
            return Promise.resolve(2 + 2).then(function(result) {
                expect(result).to.equal(4);
            })
        });
        it("chas-as-promise", function() {
            return expect(Promise.resolve(2+2)).to.eventually.equal(4);
        });
        it('multiple promises', function() {
            return Promise.all([
                expect(Promise.resolve(2)).eventually.equal(2),
                expect(Promise.resolve(3)).eventually.equal(3),
                expect(Promise.resolve(4)).eventually.equal(4),
                Promise.resolve(5).then(function(result) {
                    expect(result).to.equal(5);
                })
            ])
        })
    });
});

